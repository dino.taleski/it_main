export type ArrangementType = { 
    
        "id": number,
        "arrangementType": string,
        "country": string,
        "img": string,
        "title": string,
        "rating": number,
        "region": string,
        "location": string,
        "nights": number,
        "distanceFromBeach": number,
        "pricePerNight": number,
        "availableDate": [],
        "isLastMinute": boolean,
        "isPublished": boolean,
        "occupancy": string,
        "isAvailable": boolean,
        "lastMinutePricePerNight": number
      
}