import React, { createContext, useContext, useState } from "react";

export type GlobalContextType = {
    globalDestination: string;
    setGlobalDestination: React.Dispatch<React.SetStateAction<string>>;
};

export const GlobalContext = createContext<GlobalContextType | null>(null);

const Context = ({ children }: { children: React.ReactNode }) => {
    const [globalDestination, setGlobalDestination] = useState("");

    const contextValue: GlobalContextType = {
        globalDestination,
        setGlobalDestination,
    };

    return (
        <GlobalContext.Provider value={contextValue}>
            {children}
        </GlobalContext.Provider>
    );
};

export default Context;
