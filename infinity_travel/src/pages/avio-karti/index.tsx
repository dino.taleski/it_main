import Banner from "@/components/banner/Banner";
import PlaneTicketsForm from "@/components/planeticketsform/PlaneTicketsForm";
import SubscribeNewsletter from "@/components/subnewsletter/SubscribeNewsletter";
import { NextPage } from "next";

const PlaneTickets: NextPage = () => {


    return (
        <div >
            <Banner />
            <PlaneTicketsForm />
            <SubscribeNewsletter />
        </div>
    );
};

export default PlaneTickets;