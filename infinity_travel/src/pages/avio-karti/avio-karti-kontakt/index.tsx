import Banner from "@/components/banner/Banner";
import ComponentHeadingComp from "@/components/componentheadingcomp/ComponentHeadingComp";
import ContactUsOfferComp from "@/components/contactusoffercomp/ContactUsOfferComp";
import MainContactComp from "@/components/maincontactcomp/MainContactComp";
import SubscribeNewsletter from "@/components/subnewsletter/SubscribeNewsletter";
import { NextPage } from "next";
import React from "react";

const ContactPageForPlaneTickets: NextPage = () => {
  return (
    <>
      <Banner />
      <ComponentHeadingComp title={"Авио Карти"} />
      <div style={{ padding: "5px" }}>
        <ContactUsOfferComp />
      </div>
      <SubscribeNewsletter />
    </>
  );
};

export default ContactPageForPlaneTickets;
