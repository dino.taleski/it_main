import Banner from "@/components/banner/Banner";
import ComponentHeadingComp from "@/components/componentheadingcomp/ComponentHeadingComp";
import SubscribeNewsletter from "@/components/subnewsletter/SubscribeNewsletter";
import Card from "@/components/util/card/Card";
import { GlobalContext } from "@/context/GlobalContext";
import { ArrangementType } from "@/types/types";
import { GetServerSideProps, NextPage } from "next";
import { useRouter } from "next/router";
import React, { useContext, useEffect, useState } from "react";

type Props = {
    apartmentsData: ArrangementType[];
};

const Apartments: NextPage<Props> = ({ apartmentsData }) => {
    const [title, setTitle] = useState<string>("");

    const context = useContext(GlobalContext);
    const destination = context?.globalDestination;

    const filteredApartmentsByCountry = apartmentsData.filter(
        (data) => data.country === destination
    );

    const route = useRouter();

    useEffect(() => {
        if (destination === "greece") {
            setTitle("Грција");
        } else if (destination === "turkey") {
            setTitle("Турција");
        } else if (destination === "albania") {
            setTitle("Албанија");
        } else if (destination === "croatia") {
            setTitle("Хрватска");
        } else if (destination === "monteNegro") {
            setTitle("Црна Гора");
        } else if (destination === "italy") {
            setTitle("Италија");
        } else if (destination === "spain") {
            setTitle("Шпанија");
        }
    }, [destination]);

    function handleOfferId(offerId: number) {
        route.push({
            pathname: `apartmani/${offerId}`,
            query: {
                ...route.query,
                id: offerId,
            },
        });
    }

    return (
        <>
            <Banner />
            <ComponentHeadingComp title={`${title} хотели`} />
            <div
                style={{
                    display: "grid",
                    gridTemplateColumns: "repeat(3, 1fr",
                    gap: "50px",
                    paddingBottom: "40px",
                }}
            >
                {filteredApartmentsByCountry.map((data) => {
                    return (
                        <div
                            key={data.id}
                            onClick={() => {
                                console.log("clicked");
                                handleOfferId(data.id);
                            }}
                        >
                            <Card
                                title={data.title}
                                location={data.location}
                                distanceFromBeach={data.distanceFromBeach}
                                pricePerNight={data.pricePerNight}
                                nights={data.nights}
                            />
                        </div>
                    );
                })}
            </div>
            <SubscribeNewsletter />
        </>
    );
};

export default Apartments;

export const getServerSideProps: GetServerSideProps = async () => {
    const apartmentsResponse = await fetch(
        "https://real-tan-piglet-belt.cyclic.cloud/arrangements?arrangementType=apartment"
    );
    const apartmentsData: ArrangementType[] = await apartmentsResponse.json();

    return {
        props: {
            apartmentsData,
        },
    };
};
