import AppartmentOffers from "@/components/apartmentOffers/AppartmentOffers";
import Banner from "@/components/banner/Banner";
import ComponentHeadingComp from "@/components/componentheadingcomp/ComponentHeadingComp";
import HotelOffers from "@/components/hotelOffers/HotelOffers";
import SubscribeNewsletter from "@/components/subnewsletter/SubscribeNewsletter";
import { ArrangementType } from "@/types/types";
import { GetServerSideProps, NextPage } from "next";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

type Props = {
    hotelData: ArrangementType[];
    apartmentData: ArrangementType[];
    hotelPartialData: ArrangementType[];
    apartmentPartialData: ArrangementType[];
};

const Destination: NextPage<Props> = ({
    hotelData,
    apartmentData,
    hotelPartialData,
    apartmentPartialData,
}) => {
    const [title, setTitle] = useState<string>("");

    const route = useRouter();

    useEffect(() => {
        if (route.query.country === "greece") {
            setTitle("Грција");
        } else if (route.query.country === "turkey") {
            setTitle("Турција");
        } else if (route.query.country === "albania") {
            setTitle("Албанија");
        } else if (route.query.country === "croatia") {
            setTitle("Хрватска");
        } else if (route.query.country === "monteNegro") {
            setTitle("Црна Гора");
        } else if (route.query.country === "italy") {
            setTitle("Италија");
        } else if (route.query.country === "spain") {
            setTitle("Шпанија");
        }
    }, [route.query.country]);

    return (
        <>
            <Banner />
            <ComponentHeadingComp title={`${title} Апартмани`} />
            <AppartmentOffers
                apartmentData={apartmentData}
                apartmentPartialData={apartmentPartialData}
            />
            <ComponentHeadingComp title={`${title} Хотели`} />
            <HotelOffers
                hotelData={hotelData}
                hotelPartialData={hotelPartialData}
            />
            <SubscribeNewsletter />
        </>
    );
};

export default Destination;

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
    try {
        const country = query.country;
        let randomPart = Math.floor(Math.random() * 15);

        const hotelResponse = await fetch(
            `https://real-tan-piglet-belt.cyclic.cloud/arrangements?country=${country}&arrangementType=hotel`
        );
        const hotelData = await hotelResponse.json();

        const hotelPartialResponse = await fetch(
            `https://real-tan-piglet-belt.cyclic.cloud/arrangements?country=${country}&arrangementType=hotel&_start=${randomPart}&_limit=4`
        );
        const hotelPartialData = await hotelPartialResponse.json();

        const apartmentResponse = await fetch(
            `https://real-tan-piglet-belt.cyclic.cloud/arrangements?country=${country}&arrangementType=apartment`
        );
        const apartmentData = await apartmentResponse.json();

        const apartmentPartialResponse = await fetch(
            `https://real-tan-piglet-belt.cyclic.cloud/arrangements?country=${country}&arrangementType=apartment&_start=${randomPart}&_limit=4`
        );
        const apartmentPartialData = await apartmentPartialResponse.json();

        return {
            props: {
                hotelData,
                apartmentData,
                hotelPartialData,
                apartmentPartialData,
            },
        };
    } catch (err) {
        return {
            notFound: true,
        };
    }
};
