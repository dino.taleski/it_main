import Banner from "@/components/banner/Banner";
import ComponentHeadingComp from "@/components/componentheadingcomp/ComponentHeadingComp";
import SubscribeNewsletter from "@/components/subnewsletter/SubscribeNewsletter";
import Card from "@/components/util/card/Card";
import { GlobalContext } from "@/context/GlobalContext";
import { ArrangementType } from "@/types/types";
import { GetServerSideProps, NextPage } from "next";
import React, { useContext, useEffect, useState } from "react";

type Props = {
    hotelsData: ArrangementType[];
};

const Hotels: NextPage<Props> = ({ hotelsData }) => {
    const [title, setTitle] = useState<string>("");

    const context = useContext(GlobalContext);
    const destination = context?.globalDestination;

    const filteredHotelsByCountry = hotelsData.filter(
        (data) => data.country === destination
    );

    useEffect(() => {
        if (destination === "greece") {
            setTitle("Грција");
        } else if (destination === "turkey") {
            setTitle("Турција");
        } else if (destination === "albania") {
            setTitle("Албанија");
        } else if (destination === "croatia") {
            setTitle("Хрватска");
        } else if (destination === "monteNegro") {
            setTitle("Црна Гора");
        } else if (destination === "italy") {
            setTitle("Италија");
        } else if (destination === "spain") {
            setTitle("Шпанија");
        }
    }, [destination]);

    console.log(filteredHotelsByCountry);

    return (
        <>
            <Banner />
            <ComponentHeadingComp title={`${title} хотели`} />
            <div
                style={{
                    display: "grid",
                    gridTemplateColumns: "repeat(3, 1fr",
                    gap: "50px",
                    paddingBottom: "40px",
                }}
            >
                {filteredHotelsByCountry.map((data) => {
                    return (
                        <Card
                            key={data.id}
                            title={data.title}
                            location={data.location}
                            distanceFromBeach={data.distanceFromBeach}
                            pricePerNight={data.pricePerNight}
                            nights={data.nights}
                        />
                    );
                })}
            </div>
            <SubscribeNewsletter />
        </>
    );
};

export default Hotels;

export const getServerSideProps: GetServerSideProps = async () => {
    const hotelsResponse = await fetch(
        "http://localhost:5002/arrangements?arrangementType=hotel"
    );
    const hotelsData: ArrangementType[] = await hotelsResponse.json();

    return {
        props: {
            hotelsData,
        },
    };
};
