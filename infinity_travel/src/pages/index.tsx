import Banner from "@/components/banner/Banner";
import ComponentHeadingComp from "@/components/componentheadingcomp/ComponentHeadingComp";
import GroupTravelsLink from "@/components/grouptravelslink/GroupTravelsLink";
import Memories from "@/components/memories/Memories";
import Offers from "@/components/hotelOffers/HotelOffers";
import Statistics from "@/components/statistics/Statistics";
import SubscribeNewsletter from "@/components/subnewsletter/SubscribeNewsletter";
import Testimonials from "@/components/testimonials/Testimonials";
import { ArrangementType } from "@/types/types";
import { GetServerSideProps } from "next";
import { Inter } from "next/font/google";
import Head from "next/head";
import AttractiveOffers from "@/components/atractiveOffers/AttractiveOffers";

const inter = Inter({ subsets: ["latin"] });

type Props = {
    arrangementsData: ArrangementType[];
    partialArrangementsData: ArrangementType[];
};

export default function Home({
    arrangementsData,
    partialArrangementsData,
}: Props) {
    return (
        <>
            <Head>
                <title>Дома</title>
                <meta name="description" content="infinity travel дома" />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1"
                />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main>
                <Banner />
                <ComponentHeadingComp title={"Актуелни понуди"} />
                <AttractiveOffers />
                <GroupTravelsLink />
                <Statistics />
                <Testimonials />
                <Memories />
                <SubscribeNewsletter />
            </main>
        </>
    );
}

export const getServerSideProps: GetServerSideProps = async () => {
    try {
        let randomPart = Math.floor(Math.random() * 1000);

        const arrangementsResponse = await fetch(
            "https://real-tan-piglet-belt.cyclic.cloud/arrangements"
        );
        const arrangementsData: ArrangementType[] =
            await arrangementsResponse.json();

        const partialArrangementsResponse = await fetch(
            `https://real-tan-piglet-belt.cyclic.cloud/arrangements?_start=${randomPart}&_limit=4`
        );
        const partialArrangementsData: ArrangementType[] =
            await partialArrangementsResponse.json();

        return {
            props: {
                arrangementsData,
                partialArrangementsData,
            },
        };
    } catch (err) {
        return {
            notFound: true,
        };
    }
};
