import Banner from '@/components/banner/Banner'
import ComponentHeadingComp from '@/components/componentheadingcomp/ComponentHeadingComp'
import ContactUsOfferComp from '@/components/contactusoffercomp/ContactUsOfferComp'
import GroupTravelsContentComp from '@/components/grouptravelscontencomp/GroupTravelsContentComp'
import SubscribeNewsletter from '@/components/subnewsletter/SubscribeNewsletter'
import { NextPage } from 'next'
import React from 'react'

const GroupTravelsPage: NextPage = () => {
    return (
        <div>
            <Banner />
            <GroupTravelsContentComp />
            <ComponentHeadingComp title={'Контакт'} />
            <ContactUsOfferComp />
            <SubscribeNewsletter />
        </div>
    )
}

export default GroupTravelsPage