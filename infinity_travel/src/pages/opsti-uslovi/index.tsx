import Banner from "@/components/banner/Banner";
import { NextPage } from "next";
import React from "react";
import classes from "@/styles/tospage.module.css";
import ComponentHeadingComp from "@/components/componentheadingcomp/ComponentHeadingComp";
import SubscribeNewsletter from "@/components/subnewsletter/SubscribeNewsletter";

const TermsOfServicePage: NextPage = () => {
    return (
        <>
            <Banner />
            <ComponentHeadingComp title="Општи услови" />
            <div className={classes.mainDiv}>
                <p className={classes.heading}>Lorem Ipsum</p>
                <p className={classes.text}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmo d tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.Lorem ipsum dolor sit
                    amet, consectetur adipiscing elit, sed do eiusmod tempor
                </p>
                <p className={classes.heading}>Lorem ipsum dolor sin amet</p>
                <p className={classes.text}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmo d tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exerciLorem
                    ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmo d tempor incididunt ut labore et dolore magna aliqua.
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco
                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                    irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                    cupidatat non proident, sunt in culpa qui officia deserunt
                    mollit anim id est laborum.Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit, sed do eiusmod tempor tation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.Lorem ipsum dolor sit
                    amet, consectetur adipiscing elit, sed do eiusmod tempor
                </p>
                <p className={classes.heading}>
                    Lorem ipsum consectetur adipiscing elit, sed do eiusmo
                </p>
                <p className={classes.text}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmo d tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exerciLorem
                    ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmo d tempor incididunt ut labore et dolore magna aliqua.
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco
                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                    irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                    cupidatat non proident, sunt in culpa qui officia deserunt
                    mollit anim id est laborum.Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit, sed do eiusmod tempor tation u
                </p>
            </div>
            <SubscribeNewsletter />
        </>
    );
};

export default TermsOfServicePage;
