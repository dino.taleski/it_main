import Banner from "@/components/banner/Banner";
import ComponentHeadingComp from "@/components/componentheadingcomp/ComponentHeadingComp";
import React from "react";
import classes from "@/styles/aboutus.module.css";
import ContactUsOfferComp from "@/components/contactusoffercomp/ContactUsOfferComp";
import SubscribeNewsletter from "@/components/subnewsletter/SubscribeNewsletter";

const AboutUsPage = () => {
    return (
        <>
            <Banner />
            <ComponentHeadingComp title={"За нас"} />
            <div className={classes.aboutUsDiv}>
                <img
                    className={classes.aboutUsImg}
                    src="/images/about_us.jpg"
                    alt=""
                />
                <div className={classes.aboutUsBlurb}>
                    <img src="/images/about_us_blurbbgd.png" alt="" />
                    <div className={classes.aboutUsBlurbText}>
                        <p className={classes.heading}>Lorem Ipsum</p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit, sed do eiusmo d tempor incididunt ut labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis
                            nostrud exercitation ullamco laboris nisi ut aliquip
                            ex ea commodo consequat. Duis aute irure dolor in
                            reprehenderit in voluptate velit esse cillum dolore
                            eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat non proident, sunt in culpa qui officia
                            deserunt mollit anim id est laborum.Lorem ipsum
                            dolor sit amet, consectetur adipiscing elit, sed do
                            eiusmod tempor
                        </p>
                    </div>
                </div>
            </div>
            <div id="contact">
                <ComponentHeadingComp title={"Контакт"} />
            </div>
            <ContactUsOfferComp />
            <SubscribeNewsletter />
        </>
    );
};

export default AboutUsPage;
