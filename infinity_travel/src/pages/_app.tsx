import Footer from "@/components/footer/Footer";
import Header from "@/components/header/Header";
import Context from "@/context/GlobalContext";
import "@/styles/globals.css";
import type { AppProps } from "next/app";

export default function App({ Component, pageProps }: AppProps) {
    return (
        <>
            <Context>
                <Header />
                <Component {...pageProps} />
                <Footer />
            </Context>
        </>
    );
}
