import Banner from "@/components/banner/Banner";
import ComponentHeadingComp from "@/components/componentheadingcomp/ComponentHeadingComp";
import { NextPage } from "next";
import React from "react";
import classes from "@/styles/exoticdest.module.css";
import Card from "@/components/util/card/Card";
import SubscribeNewsletter from "@/components/subnewsletter/SubscribeNewsletter";

const ExoticDestinations: NextPage = () => {
    return (
        <>
            <Banner />
            <ComponentHeadingComp title={"Егзотични дестинации"} />
            <div className={classes.filtersDiv}>
                <button
                    className={`${classes.filtersButton} ${classes.active}`}
                >
                    Касандра
                </button>
                <button className={`${classes.filtersButton}`}>Ситонија</button>
                <button className={`${classes.filtersButton}`}>Лефкада</button>
                <button className={`${classes.filtersButton}`}>Тасос</button>
                <button className={`${classes.filtersButton}`}>
                    Last minute
                </button>
            </div>
            <div style={{ padding: "20px 0" }}>
                <div className={classes.cardsDiv}>
                    <Card />
                    <Card />
                    <Card />
                    <Card />
                    <Card />
                    <Card />
                    <Card />
                </div>
            </div>

            <SubscribeNewsletter />
        </>
    );
};

export default ExoticDestinations;
