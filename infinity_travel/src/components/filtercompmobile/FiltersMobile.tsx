import React, { useState } from "react";
import classes from "./filtercompmobile.module.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const FiltersMobile = () => {
    const [startDate, setStartDate] = useState(new Date());
    return (
        <>
            <div className={classes.mainDiv}>
                <input type="text" />
                <img src="/icons/filter_button.svg" alt="" />
                <img
                    className={classes.searchIcon}
                    src="/icons/mobile-searchicon.svg"
                    alt=""
                />
            </div>
            <div className={classes.filtersDiv}>
                <div className={classes.filtersInnerDiv}>
                    <div
                        style={{
                            display: "flex",
                            justifyContent: "space-between",
                        }}
                    >
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "column",
                                width: "100%",
                            }}
                        >
                            Од
                        </div>
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "column",
                                width: "100%",
                            }}
                        >
                            До
                        </div>
                    </div>
                </div>
                <div style={{ display: "flex", justifyContent: "center" }}>
                    <input
                        style={{ width: "90%" }}
                        type="date"
                        name="dateFrom"
                        id="dateFrom"
                    />
                </div>
            </div>
        </>
    );
};

export default FiltersMobile;
