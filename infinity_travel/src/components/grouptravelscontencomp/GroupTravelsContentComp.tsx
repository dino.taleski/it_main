import React from "react";
import classes from "./grouptravelscontencomp.module.css";
import ComponentHeadingComp from "../componentheadingcomp/ComponentHeadingComp";

const GroupTravelsContentComp: React.FC = () => {
    return (
        <div>
            <ComponentHeadingComp title={"Групни патувања"} />
            <div className={classes.groupTravelsContentDiv}>
                <div>
                    <p className={classes.groupTravelsContentHeading}>
                        Mice Tourism
                    </p>
                    <p className={classes.groupTravelsContentParagraph}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserunt mollit
                        anim id est laborum.Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad
                        minim{" "}
                    </p>
                </div>
                <div style={{ display: "flex" }}>
                    <img
                        style={{ width: "100%", alignSelf: "center" }}
                        src="/images/group_travels_1.png"
                        alt=""
                    />
                </div>
            </div>
            <div
                className={`${classes.groupTravelsContentDiv} ${classes.rowReverse}`}
            >
                <div>
                    <p className={classes.groupTravelsContentHeading}>
                        Team Building
                    </p>
                    <p className={classes.groupTravelsContentParagraph}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserunt mollit
                        anim id est laborum.Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad
                        minim{" "}
                    </p>
                </div>
                <div style={{ display: "flex" }}>
                    <img
                        style={{ width: "100%", alignSelf: "center" }}
                        src="/images/group_travels_2.png"
                        alt=""
                    />
                </div>
            </div>
            <div className={classes.groupTravelsContentDiv}>
                <div>
                    <p className={classes.groupTravelsContentHeading}>
                        Tailor made
                    </p>
                    <p className={classes.groupTravelsContentParagraph}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserunt mollit
                        anim id est laborum.Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Ut enim ad
                        minim{" "}
                    </p>
                </div>
                <div style={{ display: "flex" }}>
                    <img
                        style={{ width: "100%", alignSelf: "center" }}
                        src="/images/group_travels_3.png"
                        alt=""
                    />
                </div>
            </div>
        </div>
    );
};

export default GroupTravelsContentComp;
