import React from 'react'
import classes from './planeticketsform.module.css'
import Link from 'next/link'
import ComponentHeadingComp from '../componentheadingcomp/ComponentHeadingComp'


const PlaneTicketsForm: React.FC = () => {
    return (
        <div >
             <ComponentHeadingComp title={'Авио карти'} />
            <div className={classes.formMainDiv}>
                <form>
                    <div className={classes.formInnerDiv}>
                        <p>Пребарувај</p>
                        <div >
                            <div className={classes.formInputDiv}>
                                <div style={{ width: '100%' }}>
                                    <input type="radio" id='returnTicket' name='planeTicket' />
                                    <label htmlFor="returnTicket">Повратен билет</label>
                                </div>
                                <div style={{ width: '100%' }}>
                                    <input type="radio" id='onewayTicker' name='planeTicket' />
                                    <label htmlFor="onewayTicker">Еден правец</label>
                                </div>
                            </div>
                            <div className={classes.mainInputDiv}>
                                <div className={classes.inputDiv}>
                                    <label htmlFor="priceFrom">Од</label>
                                    <input type="number" id='priceFrom' />
                                </div>
                                <div className={classes.inputDiv}>
                                    <label htmlFor="priceTo">До</label>
                                    <input type="number" id='priceTo' />
                                </div>
                                <div className={classes.inputDiv}>
                                    <label htmlFor="fromDate">Датум на поаѓање</label>
                                    <input type="date" id='fromDate' />
                                </div>
                                <div className={classes.inputDiv}>
                                    <label htmlFor="toDate">Датум на враќање</label>
                                    <input type="date" id='toDate' />
                                </div>
                                <div className={classes.inputDiv}>
                                    <label htmlFor="parents">Возрасни</label>
                                    <select id="parents">
                                        <option value="1"></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                                <div className={classes.inputDiv}>
                                    <label htmlFor="kids">Деца</label>
                                    <select id="kids">
                                        <option value="1"></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                                <div className={classes.inputDiv}>
                                    <label htmlFor="babies">Бебиња</label>
                                    <select id="babies">
                                        <option value="1"></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                                <div className={classes.inputDiv}>
                                    <label htmlFor="class">Класа</label>
                                    <select id="class">
                                        <option value="1">Економска</option>
                                        <option value="1">Бизнис</option>
                                        <option value="2">Бизнис +</option>
                                        <option value="3">Прва</option>
                                        <option value="4">Прва Премиум</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <Link href={'/avio-karti/avio-karti-kontakt'}><button className={classes.postFormButton}>ПОБАРАЈ ПОНУДА</button></Link>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default PlaneTicketsForm