import React from "react";
import classes from "./card.module.css";
import { useRouter } from "next/router";

type CardType = {
    title: string;
    location: string;
    distanceFromBeach: number;
    pricePerNight: number;
    nights: number;
};

const Card: React.FC<CardType> = ({
    title,
    location,
    distanceFromBeach,
    pricePerNight,
    nights,
}) => {
    return (
        <div className={classes.cardDiv}>
            <img
                className={classes.cardImg}
                src="/images/vila-image.jpg"
                alt="vacation vila image"
            />
            <div className={classes.cardContent}>
                <div className={classes.titleAndRating}>
                    <p>{title}</p>
                    <p>***</p>
                </div>
                <p>icon/{location}</p>
                <div
                    style={{ display: "flex", justifyContent: "space-between" }}
                >
                    <div className={classes.stayDur}>
                        <p>10 ноќевања/наем соба</p>
                        <p>{distanceFromBeach}m</p>
                    </div>
                    <div className={classes.stayPrice}>
                        <p style={{ textAlign: "end" }}>од</p>
                        <p>€{pricePerNight * 10}</p>
                    </div>
                </div>
            </div>
            <button className={classes.moreButton}>ПОВЕЌЕ</button>
        </div>
    );
};

export default Card;
