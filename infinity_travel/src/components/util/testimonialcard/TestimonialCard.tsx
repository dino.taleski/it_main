import React from 'react'
import classes from './testimonialcard.module.css'

const TestimonialCard = () => {
    return (
        <div>
            <div className={classes.mainCardDiv}>
                <img className={classes.yellowTape} src="./assets/yellowtape.png" alt="" />
                <div className={classes.imageDiv}>
                    <img className={classes.maskImage} src="./images/hotelimage.jpg" alt="" />
                </div>
                <div className={classes.content}>
                    <p className={classes.heading}>Lorem ipsum</p>
                    <p>*****</p>
                    <p className={classes.description}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>
                    <p className={classes.hotelName}>Marriot Hotel</p>
                    <img className={classes.streak} src="./assets/streak.svg" alt="" />
                </div>
            </div>
        </div>
    )
}

export default TestimonialCard