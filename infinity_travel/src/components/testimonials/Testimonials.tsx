import React from 'react'
import classes from './testimonials.module.css'
import TestimonialCard from '../util/testimonialcard/TestimonialCard'
import ComponentHeadingComp from '../componentheadingcomp/ComponentHeadingComp'

const Testimonials: React.FC = () => {
    return (
        <>
            <ComponentHeadingComp title={'Тестимониал'} />
            <div className={classes.parent}>
                <div className={classes.mainDiv}>
                    <div className={classes.mainDivInner}>
                        <TestimonialCard />
                        <TestimonialCard />
                        <TestimonialCard />
                        <TestimonialCard />
                    </div>
                </div>
            </div>
        </>
    )
}

export default Testimonials