import React from "react";
import classes from "./footer.module.css";
import Link from "next/link";

const Footer: React.FC = () => {
    return (
        <>
            <div className={classes.compDisplay}>
                <div>
                    <p className={classes.footerHeading}>Дестинации</p>
                    <ul className={`${classes.ulReset} ${classes.links}`}>
                        <li>Грција</li>
                        <li>Турција</li>
                        <li>Црна Гора</li>
                        <li>Хрватска</li>
                        <li>Египет</li>
                        <li>Италија</li>
                        <li>Егзотични патувања</li>
                    </ul>
                </div>
                <div>
                    <p className={classes.footerHeading}>Информации</p>
                    <ul className={`${classes.ulReset} ${classes.links}`}>
                        <li>Авио карти</li>
                        <li>MICE Туризам</li>
                        <li>Team building</li>
                        <li>Tailor made</li>
                        <li>Gift card</li>
                    </ul>
                </div>
                <div>
                    <p className={classes.footerHeading}>Останато</p>
                    <ul className={`${classes.ulReset} ${classes.links}`}>
                        <Link
                            style={{ textDecoration: "none", color: "black" }}
                            href="za-nas"
                        >
                            <li>За нас</li>
                        </Link>
                        <Link
                            style={{ textDecoration: "none", color: "black" }}
                            href="opsti-uslovi"
                        >
                            <li>Општи услови за патување</li>
                        </Link>
                        <Link
                            style={{ textDecoration: "none", color: "black" }}
                            href="patnicko-osiguruvanje "
                        >
                            <li>Патничко осигурување</li>
                        </Link>
                    </ul>
                </div>
                <div>
                    <p className={classes.footerHeading}>Контакт</p>
                    <ul className={`${classes.ulReset} ${classes.contact}`}>
                        <div>
                            <li>
                                Адреса: Бул. Даме Груев бр.14 лок. 24 1000
                                Скопје, Македонија
                            </li>
                            <li>Е-маил: contact@infinitytravel.mk</li>
                            <li>Телефон: 023100360/072254160</li>
                        </div>
                        <div style={{ alignSelf: "end" }}>
                            <li style={{ display: "flex" }}>
                                <i
                                    className={`fa-brands fa-facebook ${classes.socialIcons}`}
                                ></i>
                                <i
                                    className={`fa-brands fa-instagram ${classes.socialIcons}`}
                                ></i>
                            </li>
                        </div>
                    </ul>
                </div>
            </div>
            <div className={classes.copyrightBgd}>
                <div className={classes.copyright}>
                    <p>&copy; 2019 Инфинити травел. Сите права се задржани</p>
                </div>
            </div>
        </>
    );
};

export default Footer;
