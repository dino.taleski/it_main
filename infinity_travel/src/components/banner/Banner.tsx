import React from 'react'
import classes from './banner.module.css'

const Banner: React.FC = () => {
    return (
        <div style={{ position: "relative" }}>
            <img className={classes.bannerImg} src="/images/banner-image-1.jpg" alt="banner image travel" />
            <div className={classes.searchMainContainer}>
                <div style={{ position: "relative" }}>
                    <img className={classes.searchVector} src="/assets/banner-search-vector.png" alt="search banner" />
                    <div className={classes.searchInputContainer}>
                        <p className={classes.searchHeading} >Lorem ipsum dolor sit amet consectetur.</p>
                        <p className={classes.searchParagraph} >Lorem ipsum dolor sit amet consectetur, sti amet.</p>
                        <div className={classes.searchInputDiv}>
                            <input className={classes.searchInput} type="text" />
                            <img className={classes.searchInputButton} src="/assets/search-vector.png" alt="search button" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Banner