import classes from "@/components/hotelOffers/offers.module.css";
import { ArrangementType } from "@/types/types";
import Link from "next/link";
import React, { useState } from "react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import { A11y, Navigation, Pagination, Scrollbar } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";
import Card from "../util/card/Card";

type Props = {
    apartmentData: ArrangementType[];
    apartmentPartialData: ArrangementType[];
};

const AppartmentOffers: React.FC<Props> = ({
    apartmentData,
    apartmentPartialData,
}) => {
    const [region, setRegion] = useState("");
    const [showLastMinute, setShowLastMinute] = useState(false);

    const regions = new Set(apartmentData.map((apt) => apt.region));

    const filteredApartmentData = apartmentData.filter(
        (aptDat) => aptDat.region === region
    );

    const filterLastMinuteData = apartmentData.filter(
        (aptData) => aptData.isLastMinute === true
    );

    let renderData = showLastMinute
        ? filterLastMinuteData
        : filteredApartmentData;

    return (
        <>
            <div className={classes.filtersDiv}>
                {Array.from(regions).map((reg) => {
                    return (
                        <button
                            className={`${classes.filtersButton}`}
                            key={reg}
                            onClick={() => {
                                setRegion(reg);
                                setShowLastMinute(false);
                            }}
                        >
                            {reg}
                        </button>
                    );
                })}

                <button
                    onClick={() => {
                        setShowLastMinute(!showLastMinute);
                    }}
                    className={`${classes.filtersButton}`}
                >
                    Last minute
                </button>
            </div>

            <Swiper
                spaceBetween={50}
                slidesPerView={4}
                modules={[Navigation, Pagination, Scrollbar, A11y]}
                navigation
                className={classes.desktopCarousel}
            >
                {renderData.map((data) => {
                    return (
                        <SwiperSlide key={data.id}>
                            <Card
                                title={data.title}
                                location={data.location}
                                distanceFromBeach={data.distanceFromBeach}
                                pricePerNight={data.pricePerNight}
                                nights={data.nights}
                            />
                        </SwiperSlide>
                    );
                })}
            </Swiper>
            <div className={classes.cardsDiv}>
                {apartmentPartialData.map((aptPartDat) => {
                    return (
                        <Card
                            key={aptPartDat.id}
                            title={aptPartDat.title}
                            location={aptPartDat.location}
                            distanceFromBeach={aptPartDat.distanceFromBeach}
                            pricePerNight={aptPartDat.pricePerNight}
                            nights={aptPartDat.nights}
                        />
                    );
                })}
            </div>
            <div className={classes.seeMoreDiv}>
                <Link
                    className={classes.seeMoreLink}
                    href={"/destinacii/apartmani/"}
                >
                    <div>{`See more > >`}</div>
                </Link>
            </div>
        </>
    );
};

export default AppartmentOffers;
