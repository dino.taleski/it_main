import React from "react";
import classes from "./componentheadingcomp.module.css";

type Props = {
  title: string
}

const ComponentHeadingComp: React.FC<Props> = ({title}) => {

  return (
    <>
      <div className={classes.linesDiv}>
        <img className={classes.lines} src="/assets/Line.png" alt="" />
        <p className={classes.title}>{title}</p>
        <img className={classes.lines} src="/assets/Line.png" alt="" />
      </div>
    </>
  );
};

export default ComponentHeadingComp;
