import React, { useContext, useRef, useState } from "react";
import classes from "./header.module.css";
import Link from "next/link";
import { useRouter } from "next/router";
import { GlobalContext } from "@/context/GlobalContext";

const Header: React.FC = () => {
    const [toggleDropDown, setToggleDropDown] = useState<boolean>(false);
    const [showMobileMenu, setShowMobileMenu] = useState<boolean>(false);

    const context = useContext(GlobalContext);

    const setGlobalDestination = context?.setGlobalDestination;

    const route = useRouter();

    function handleDestination(dest: string) {
        route.push({
            pathname: `/destinacii/${dest}`,
            query: {
                country: dest,
            },
        });
    }

    function handleGlobalDestination(dest: string) {
        route.push({
            pathname: `/destinacii/hoteli/povekje`,
            query: {
                globCountry: dest,
            },
        });
    }

    function handleSetGlobalDestination(destination: string) {
        if (setGlobalDestination) {
            setGlobalDestination(destination);
        }
    }

    return (
        <div className={classes.header}>
            <Link href={"/"}>
                <img src="/logo/ITlogo.png" alt="" />
            </Link>
            <div
                className={classes.mobileMenuIcons}
                style={{ display: "flex" }}
            >
                <i
                    className={`fa-solid fa-magnifying-glass ${classes.search}`}
                ></i>
                <i
                    onClick={() =>
                        setShowMobileMenu((showMobileMenu) => !showMobileMenu)
                    }
                    className={`fa-solid fa-bars ${classes.burgerMenuIcon}`}
                ></i>
            </div>
            <ul className={classes.desktopMenu}>
                <li>Дома</li>
                <li
                    style={{ position: "relative" }}
                    onClick={() =>
                        setToggleDropDown((toggleDropDown) => !toggleDropDown)
                    }
                >
                    Дестинации
                    <img
                        className={
                            !toggleDropDown ? classes.rotate0 : classes.rotate90
                        }
                        style={{ width: "6px", marginLeft: "5px" }}
                        src="/assets/dropdown_vector.svg"
                        alt=""
                    />
                    <ul
                        className={`${
                            !toggleDropDown ? classes.hide : classes.show
                        } ${classes.desktopDropdownMenu}`}
                    >
                        <Link
                            style={{ textDecoration: "none", color: "black" }}
                            href={`/destinacii/grcija`}
                            onClick={() => {
                                handleDestination("greece");
                                handleSetGlobalDestination("greece");
                            }}
                        >
                            <li>Грција</li>
                        </Link>
                        <Link
                            style={{ textDecoration: "none", color: "black" }}
                            href={`/destinacii/turcija`}
                            onClick={() => {
                                handleDestination("turkey");
                                handleSetGlobalDestination("turkey");
                            }}
                        >
                            <li>Турција</li>
                        </Link>
                        <Link
                            style={{ textDecoration: "none", color: "black" }}
                            href={`/destinacii/albanija`}
                            onClick={() => {
                                handleDestination("albania");
                                handleSetGlobalDestination("albania");
                            }}
                        >
                            <li>Албанија</li>
                        </Link>
                        <Link
                            style={{ textDecoration: "none", color: "black" }}
                            href={`/destinacii/hrvatska`}
                            onClick={() => {
                                handleDestination("croatia");
                                handleSetGlobalDestination("croatia");
                            }}
                        >
                            <li>Хрватска</li>
                        </Link>
                        <Link
                            style={{ textDecoration: "none", color: "black" }}
                            href={`/destinacii/crnagora`}
                            onClick={() => {
                                handleDestination("monteNegro");
                                handleSetGlobalDestination("monteNegro");
                            }}
                        >
                            <li>Црна Гора</li>
                        </Link>
                        <Link
                            style={{ textDecoration: "none", color: "black" }}
                            href={`/destinacii/italija`}
                            onClick={() => {
                                handleDestination("italy");
                                handleSetGlobalDestination("italy");
                            }}
                        >
                            <li>Италија</li>
                        </Link>
                        <Link
                            style={{ textDecoration: "none", color: "black" }}
                            href={`/destinacii/spanija`}
                            onClick={() => {
                                handleDestination("spain");
                                handleSetGlobalDestination("spain");
                            }}
                        >
                            <li>Шпанија</li>
                        </Link>
                    </ul>
                </li>
                <li>Групни патувања </li>
                <li>Авио карти</li>
                <li>Истражи ја Македонија</li>
                <li>За нас</li>
                <li>02/3100-360</li>
            </ul>
            <div
                className={`${classes.mobileMenu} ${
                    !showMobileMenu ? classes.hide : classes.show
                }`}
            >
                <ul style={{ listStyle: "none" }}>
                    <li
                        onClick={() =>
                            setToggleDropDown(
                                (toggleDropDown) => !toggleDropDown
                            )
                        }
                    >
                        Дестинации{" "}
                        <img
                            className={
                                !toggleDropDown
                                    ? classes.rotate0
                                    : classes.rotate90
                            }
                            style={{ width: "6px", marginLeft: "5px" }}
                            src="/assets/dropdown_vector.svg"
                            alt=""
                        />
                        <ul
                            style={{ listStyle: "none" }}
                            className={
                                !toggleDropDown ? classes.hide : classes.show
                            }
                        >
                            <li>Грција</li>
                            <li>Турција</li>
                            <li>Албанија</li>
                            <li>Хрватска</li>
                            <li>Црна Гора</li>
                            <li>Италија</li>
                            <li>Шпанија</li>
                            <Link
                                style={{
                                    textDecoration: "none",
                                    color: "black",
                                }}
                                href={"/egzoticni-destinacii"}
                            >
                                <li>Егзотични дестинации</li>
                            </Link>
                        </ul>
                    </li>
                    <Link
                        style={{ textDecoration: "none", color: "black" }}
                        href={"/grupni-patuvanja"}
                    >
                        <li>Групни патувања</li>
                    </Link>
                    <Link
                        style={{ textDecoration: "none", color: "black" }}
                        href={"/avio-karti"}
                    >
                        <li>Авио карти</li>
                    </Link>
                    <Link
                        style={{ textDecoration: "none", color: "black" }}
                        href={"/istrazi-ja-makedonija"}
                    >
                        <li>Истражи ја Македонија</li>
                    </Link>
                    <Link
                        style={{ textDecoration: "none", color: "black" }}
                        href={"/za-nas"}
                    >
                        <li>За нас</li>
                    </Link>
                    <Link
                        style={{ textDecoration: "none", color: "black" }}
                        href={"/za-nas/#contact"}
                    >
                        <li>Контакт</li>
                    </Link>
                </ul>
            </div>
        </div>
    );
};

export default Header;
