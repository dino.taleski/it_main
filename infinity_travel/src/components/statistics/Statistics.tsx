import React from "react";
import classes from "./statistics.module.css";

const Statistics: React.FC = () => {
    return (
        <div className={classes.parent}>
            <div className={classes.mainDiv}>
                <img
                    className={classes.rippedPaperImg}
                    src="./assets/rippedpaper.png"
                    alt="facts paper"
                />
                <img
                    className={classes.shadowImage}
                    src="./assets/Vector.png"
                    alt="facts paper"
                />
                <img
                    className={classes.purpleTapeTopLeft}
                    src="./assets/purpletape.png"
                    alt=""
                />
                <img
                    className={classes.purpleTapeBottomRight}
                    src="./assets/purpletape.png"
                    alt=""
                />
                <div className={classes.statistics}>
                    <div>
                        <img src="./icons/suitcaseicon.png" alt="" />
                        <p>1000+ патувања</p>
                    </div>
                    <div>
                        <img src="./icons/worldicon.png" alt="" />
                        <p>15000+ патници годишно</p>
                    </div>
                    <div>
                        <img src="./icons/locationicon.png" alt="" />
                        <p>Одберете ја вашата дестинација</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Statistics;
