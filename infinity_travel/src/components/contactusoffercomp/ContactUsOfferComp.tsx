import React from 'react'
import classes from './contactusoffercomp.module.css'
import ComponentHeadingComp from '../componentheadingcomp/ComponentHeadingComp'

const ContactUsOfferComp = () => {
    return (
        <div>
            <div className={classes.contactFormDiv}></div>
            <div className={classes.contactInfoDiv}>
                <div className={classes.contactInfo}>
                    <div style={{ width: '100% ' }}>
                        <label htmlFor="name">Име</label>
                        <input style={{ marginTop: '5px' }} type="text" id='name' />
                    </div>
                    <div style={{ width: '100% ' }}>
                        <label htmlFor="email">Е-маил</label>
                        <input style={{ marginTop: '5px' }} type="email" id='email' />
                    </div>
                </div>
                <div className={classes.messageDiv}>
                    <label htmlFor="message">Порака</label>
                    <textarea style={{ marginTop: '5px' }} name="message" id="message" cols={30} rows={10} ></textarea>
                </div>
                <button className={classes.postOfferButton}>ПОБАРАЈ ПОНУДА</button>
            </div>
        </div>
    )
}

export default ContactUsOfferComp