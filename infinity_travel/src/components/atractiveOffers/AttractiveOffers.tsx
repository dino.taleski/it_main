import classes from "@/components/hotelOffers/offers.module.css";
import Link from "next/link";
import React from "react";
import { A11y, Navigation, Pagination, Scrollbar } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import Card from "../util/card/Card";
import "swiper/swiper-bundle.css";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import { ArrangementType } from "@/types/types";

const AttractiveOffers: React.FC = ({}) => {
    return (
        <>
            <div className={classes.filtersDiv}>
                <button
                    className={`${classes.filtersButton} ${classes.active}`}
                >
                    Касандра
                </button>
                <button className={`${classes.filtersButton}`}>Ситонија</button>
                <button className={`${classes.filtersButton}`}>Лефкада</button>
                <button className={`${classes.filtersButton}`}>Тасос</button>
                <button className={`${classes.filtersButton}`}>
                    Last minute
                </button>
            </div>

            <Swiper
                spaceBetween={50}
                slidesPerView={4}
                modules={[Navigation, Pagination, Scrollbar, A11y]}
                navigation
                className={classes.desktopCarousel}
            >
                <SwiperSlide>
                    <Card />
                </SwiperSlide>
                <SwiperSlide>
                    <Card />
                </SwiperSlide>
                <SwiperSlide>
                    <Card />
                </SwiperSlide>
                <SwiperSlide>
                    <Card />
                </SwiperSlide>
                <SwiperSlide>
                    <Card />
                </SwiperSlide>
                <SwiperSlide>
                    <Card />
                </SwiperSlide>
            </Swiper>
            <div className={classes.cardsDiv}>
                <Card />
                <Card />
                <Card />
                <Card />
            </div>
            <div className={classes.seeMoreDiv}>
                <Link className={classes.seeMoreLink} href={"/grcija/see-more"}>
                    <div>{`See more > >`}</div>
                </Link>
            </div>
        </>
    );
};

export default AttractiveOffers;
