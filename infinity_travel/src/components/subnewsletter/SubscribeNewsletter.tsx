import React, { useState } from "react";
import classes from "./subscribenewsletter.module.css";

const SubscribeNewsletter: React.FC = () => {
    const [toggleForm, setToggleForm] = useState<boolean>(true);

    return (
        <>
            <div className={classes.mainDiv}>
                <img src="/icons/mailicon.png" alt="" />
                <p>Пријави се и добивај актуелни понуди на твојот маил</p>
                <i
                    onClick={() => setToggleForm((toggleForm) => !toggleForm)}
                    className={`fa-solid fa-chevron-${
                        toggleForm ? "down" : "up"
                    }`}
                ></i>
            </div>
            <div style={{ display: toggleForm ? "none" : "block" }}>
                <div className={classes.formDiv}>
                    <label htmlFor="name">Име</label>
                    <input type="text" id="name" />
                    <label htmlFor="email">Е-маил</label>
                    <input type="text" id="email" />
                    <button>Пријави ме</button>
                    <p>
                        Со кликнување на Пријави ме се зачленуваш за добивање на
                        маилови за нашите актуелни понуди и промоции
                    </p>
                </div>
            </div>
        </>
    );
};

export default SubscribeNewsletter;
