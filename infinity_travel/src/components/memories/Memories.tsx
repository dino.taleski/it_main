import React from "react";
import classes from "./memories.module.css";
import ComponentHeadingComp from "../componentheadingcomp/ComponentHeadingComp";

const Memories: React.FC = () => {
    return (
        <div>
            <ComponentHeadingComp title={"Ваши моменти"} />
            <div className={classes.memoriesMainDiv}>
                <div style={{ width: "100%" }}>
                    <img
                        style={{
                            width: "90px",
                            height: "90px",
                            objectFit: "cover",
                        }}
                        src="./images/trip1.jpg"
                        alt=""
                    />
                </div>
                <div style={{ width: "100%" }}>
                    <img
                        style={{
                            width: "90px",
                            height: "90px",
                            objectFit: "cover",
                        }}
                        src="./images/trip2.jpg"
                        alt=""
                    />
                </div>
                <div style={{ width: "100%" }}>
                    <img
                        style={{
                            width: "90px",
                            height: "90px",
                            objectFit: "cover",
                        }}
                        src="./images/trip3.jpg"
                        alt=""
                    />
                </div>
                <div style={{ width: "100%" }}>
                    <img
                        style={{
                            width: "90px",
                            height: "90px",
                            objectFit: "cover",
                        }}
                        src="./images/trip4.jpg"
                        alt=""
                    />
                </div>
                <div style={{ width: "100%" }}>
                    <img
                        style={{
                            width: "90px",
                            height: "90px",
                            objectFit: "cover",
                        }}
                        src="./images/trip5.jpg"
                        alt=""
                    />
                </div>
                <div style={{ width: "100%" }}>
                    <img
                        style={{
                            width: "90px",
                            height: "90px",
                            objectFit: "cover",
                        }}
                        src="./images/trip6.jpg"
                        alt=""
                    />
                </div>
                <div style={{ width: "100%" }}>
                    <img
                        style={{
                            width: "90px",
                            height: "90px",
                            objectFit: "cover",
                        }}
                        src="./images/trip7.jpg"
                        alt=""
                    />
                </div>
                <div style={{ width: "100%" }}>
                    <img
                        style={{
                            width: "90px",
                            height: "90px",
                            objectFit: "cover",
                        }}
                        src="./images/trip8.jpg"
                        alt=""
                    />
                </div>
                <div style={{ width: "100%" }}>
                    <img
                        style={{
                            width: "90px",
                            height: "90px",
                            objectFit: "cover",
                        }}
                        src="./images/trip9.jpg"
                        alt=""
                    />
                </div>
                <div style={{ width: "100%" }}>
                    <img
                        style={{
                            width: "90px",
                            height: "90px",
                            objectFit: "cover",
                        }}
                        src="./images/trip10.jpg"
                        alt=""
                    />
                </div>
                <div style={{ width: "100%" }}>
                    <img
                        style={{
                            width: "90px",
                            height: "90px",
                            objectFit: "cover",
                        }}
                        src="./images/trip11.jpg"
                        alt=""
                    />
                </div>
                <div style={{ width: "100%" }}>
                    <img
                        style={{
                            width: "90px",
                            height: "90px",
                            objectFit: "cover",
                        }}
                        src="./images/trip12.jpg"
                        alt=""
                    />
                </div>
            </div>
        </div>
    );
};

export default Memories;
