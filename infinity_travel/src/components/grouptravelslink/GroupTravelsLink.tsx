import React from "react";
import classes from "./grouptravelslink.module.css";
import Link from "next/link";

const GroupTravelsLink: React.FC = () => {
    return (
        <>
            <div className={classes.mainDiv}>
                <img
                    className={classes.gourpTravelsBgd}
                    src="./assets/group_travels_bgd.png"
                    alt=""
                />
                <img
                    className={classes.londonStamp}
                    src="./assets/londonstamp.png"
                    alt=""
                />
                <img
                    className={classes.instanbulStamp}
                    src="./assets/instanbulstamp.png"
                    alt=""
                />
                <img
                    className={classes.curvedLines}
                    src="./assets/curvedlines.png"
                    alt=""
                />
                <div className={classes.primaryPicDiv}>
                    <img
                        className={classes.planeImg}
                        src="./images/planeimage.png"
                        alt=""
                    />
                    <img
                        className={classes.officeImg}
                        src="./images/officeimage.png"
                        alt=""
                    />
                </div>
                <div className={classes.secondaryPicDIv}>
                    <img
                        className={classes.mapImg}
                        src="./images/mapimage.png"
                        alt=""
                    />
                    <img
                        className={classes.officeImg2}
                        src="./images/officeimage2.png"
                        alt=""
                    />
                </div>
                <div className={classes.groupTravelMore}>
                    <div className={classes.groupTravelMoreInner}>
                        <img
                            className={classes.groupTravelMoreVector}
                            src="./assets/group_travel_vector.png"
                            alt=""
                        />
                        <div className={classes.groupTravelMoreContent}>
                            <p className={classes.groupTravelHeading}>
                                Групни патувања
                            </p>
                            <p className={classes.groupTraveParagraph}>
                                Lorem ipsum dolor sit amet consectetur
                                adipisicing elit.
                            </p>
                            <Link href={"/grupni-patuvanja"}>
                                <button
                                    className={classes.groupTravelMoreButton}
                                >
                                    ПОВЕЌЕ
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default GroupTravelsLink;
