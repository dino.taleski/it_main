import classes from "@/components/hotelOffers/offers.module.css";
import Link from "next/link";
import React, { useState } from "react";
import { A11y, Navigation, Pagination, Scrollbar } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";
import Card from "../util/card/Card";
import "swiper/swiper-bundle.css";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import { ArrangementType } from "@/types/types";

type Props = {
    hotelData: ArrangementType[];
    hotelPartialData: ArrangementType[];
};

const HotelOffers: React.FC<Props> = ({ hotelData, hotelPartialData }) => {
    const [region, setRegion] = useState("");
    const [showLastMinute, setShowLastMinute] = useState(false);

    const regions = new Set(hotelData.map((hot) => hot.region));

    const filteredHotelsRegionData = hotelData.filter(
        (hotDat) => hotDat.region === region
    );
    const filteredHotelLastMinuteData = hotelData.filter(
        (hotDat) => hotDat.isLastMinute === true
    );

    let renderHotelData = showLastMinute
        ? filteredHotelLastMinuteData
        : filteredHotelsRegionData;

    return (
        <>
            <div className={classes.filtersDiv}>
                {Array.from(regions).map((reg) => {
                    return (
                        <button
                            key={reg}
                            className={`${classes.filtersButton} `}
                            onClick={() => {
                                setRegion(reg);
                                setShowLastMinute(false);
                            }}
                        >
                            {reg}
                        </button>
                    );
                })}
                <button
                    className={`${classes.filtersButton}`}
                    onClick={() => {
                        setShowLastMinute(!showLastMinute);
                    }}
                >
                    Last minute
                </button>
            </div>

            <Swiper
                spaceBetween={50}
                slidesPerView={4}
                modules={[Navigation, Pagination, Scrollbar, A11y]}
                navigation
                className={classes.desktopCarousel}
            >
                {renderHotelData.map((data) => {
                    return (
                        <SwiperSlide key={data.id}>
                            <Card
                                title={data.title}
                                location={data.location}
                                distanceFromBeach={data.distanceFromBeach}
                                pricePerNight={data.pricePerNight}
                                nights={data.nights}
                            />
                        </SwiperSlide>
                    );
                })}
            </Swiper>
            <div className={classes.cardsDiv}>
                {hotelPartialData.map((data) => {
                    return (
                        <Card
                            key={data.id}
                            title={data.title}
                            location={data.location}
                            distanceFromBeach={data.distanceFromBeach}
                            pricePerNight={data.pricePerNight}
                            nights={data.nights}
                        />
                    );
                })}
            </div>
            <div className={classes.seeMoreDiv}>
                <Link
                    className={classes.seeMoreLink}
                    href={"/destinacii/hoteli"}
                >
                    <div>{`See more > >`}</div>
                </Link>
            </div>
        </>
    );
};

export default HotelOffers;
